import menu.MenuUtils;
import model.Housebreaker;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

public class Tests {

    @Test
    public void testProps() throws IOException {

        File file = MenuUtils.getFileFromResources("game_props.properties");
        FileInputStream propFile = new FileInputStream(file);
        Properties props = new Properties();
        props.load(propFile);

        String life = props.getProperty("max_character_life");
        assertEquals("3", life);
    }

    @Test
    public void testReadCharacterSheet() throws IOException {
        Housebreaker housebreaker = new Housebreaker();
        housebreaker.readCharacterSheet();

    }

}
