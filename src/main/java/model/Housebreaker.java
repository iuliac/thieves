package model;

import menu.MenuUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Housebreaker implements GameCharacter {

    private List<String> goods = new ArrayList<>();


    @Override
    public void steal() throws IOException {
        MenuUtils.displayListOfGoodsForStealing();
        double weight = 0;
        double remaining = 0;
        while (weight < 15) {
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
            String choise = in.readLine();

            switch (choise) {
                case "1":
                    weight = weight + 10;
                    remaining = 15 - weight;
                    goods.add("1x TV");
                    System.out.println("You can carry " + remaining + " kg more");
                    break;
                case "2":
                    weight = weight + 2.5;
                    remaining = 15 - weight;
                    goods.add("1x laptop");
                    System.out.println("You can carry " + remaining + " kg more");
                    break;
                case "3":
                    weight = weight + 1;
                    remaining = 15 - weight;
                    goods.add("1x camera");
                    System.out.println("You can carry " + remaining + " kg more");
                    break;
                case "4":
                    weight = weight + 0.25;
                    remaining = 15 - weight;
                    goods.add("1x mobile phone");
                    System.out.println("You can carry " + remaining + " kg more");
                    break;
                case "5":
                    weight = weight + 0.5;
                    remaining = 15 - weight;
                    goods.add("1x jewellery box");
                    System.out.println("You can carry " + remaining + " kg more");
                    break;
                default:
                    System.out.println("Please select a valid option");
            }

            if (weight >= 13 && weight < 15) {
                System.out.println("Warning!");
            } else if (weight == 15) {
                System.out.println("You have reached the maximum weight allowed. Please exit the house!");
                break;
            } else if (weight > 15) {
                System.out.println("You can no longer steal from this house, maximum weight is 15.");
                break;
            }
        }
        System.out.println("You stole the following goods from the house: "+ Arrays.toString(new List[]{goods}));
       // System.out.println("You stole the following goods from the house: "+ goods);
    }


    @Override
    public void escape() {

    }

    @Override
    public void open() {

    }

    @Override
    public void sleep() {

    }

    public void readCharacterSheet() throws IOException {
        File file = MenuUtils.getFileFromResources("housebreaker_sheet.csv");
        FileReader reader = new FileReader(file);
        BufferedReader in = new BufferedReader(reader);
        String line = in.readLine();
        while (line != null) {
            System.out.println(line);
            line = in.readLine();
        }
        in.close();

    }

    public static void openCharacterSheet() throws IOException {
        System.out.println("Press ENTER to see your character details:");
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        String enter = input.readLine();
        if (enter.equals("")) {
            Housebreaker housebreaker = new Housebreaker();
            housebreaker.readCharacterSheet();
        } else System.out.println("Please press ENTER key.");
    }
}
