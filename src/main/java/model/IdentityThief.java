package model;

import java.io.IOException;

public class IdentityThief implements GameCharacter {
    @Override
    public void sleep() {

    }

    @Override
    public void steal() throws IOException {

    }

    @Override
    public void escape() {

    }

    @Override
    public void open() {

    }
}
