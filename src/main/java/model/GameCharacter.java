package model;

import java.io.IOException;

public interface GameCharacter {

    void sleep();
    void steal() throws IOException;
    void escape();
    void open();
}
