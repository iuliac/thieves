package menu;

import java.io.*;


public class Main {

    public static void main(String[] args) throws IOException {

        System.out.println("Welcome to Thieves Game!");
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        MenuUtils.displayMenu();
        boolean playing = true;
        while (playing) {
            String userMenuSelection = input.readLine();

            if (userMenuSelection.equals("0")) {
                System.out.println("Exit");
                break;
            }
            switch (userMenuSelection) {
                case "1":
                    MenuUtils.displayCharacterMenu();
                    MenuUtils.chooseCharacter();
                    break;
                case "2":
                    System.out.println();
                    break;
                case "3":
                    System.out.println();
                    break;
                case "4":
                    MenuUtils.confirmGameQuit();
                    break;
                default:
                    System.out.println("Please select a valid option");
                    MenuUtils.displayMenu();
            }
        }
    }
}
