package menu;

import model.Housebreaker;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class MenuUtils {
    public static void displayMenu() {
        System.out.println("Menu: ");
        System.out.println("\t" +
                "1. New Game\n\t" +
                "2. Continue\n\t" +
                "3. Save Game\n\t" +
                "4. Quit");
    }

    public static void confirmGameQuit() throws IOException {
        System.out.println("Are you sure you want to quit the game? YES/NO");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String userChoise = in.readLine();
        if (userChoise.toLowerCase().equals("yes")) {
            System.out.println("You have exit the game!");
        } else if (userChoise.toLowerCase().equals("no")) {
            MenuUtils.displayMenu();
        }
    }

    public static void displayCharacterMenu() throws IOException {
        System.out.println("Please choose your character: ");
        System.out.println("\t" +
                "1. Housebreaker\n\t" +
                "2. Pickpocket\n\t" +
                "3. Identity thief\n\t"+ //tricks people into giving away personal information and then uses this for stealing money from their bank accounts and others
                "4. Honest thief"); //earns money playing quizes

    }

    public static void chooseCharacter() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String charcaterChoise = in.readLine();
        switch (charcaterChoise) {
            case ("1"):
                System.out.println("You have chosen Billy, the Housebreaker.");
                Housebreaker.openCharacterSheet();
                break;
            case ("2"):
                System.out.println("You have chosen Parker, the Pickpocket.");
                break;
            case ("3"):
                System.out.println("You have chosen Rosa, the Identity thief.");
                break;
            case ("4"):
                System.out.println("You have chosen Max, the Honest thief.");
        }
    }

    public static void displayListOfGoodsForStealing() {
        System.out.println("You can steal the following from this house: TVs(10kg each), laptops(2.5kg each), cameras(1kg each), mobile phones(0.25kg each) and jewelleries(0.5kg each box)");
        System.out.println("Maximum weight you can carry is 15kg.");
        System.out.println("Choose one good at a time: ");
        System.out.println("\t" +
                "1. TV\n\t" +
                "2. laptop\n\t" +
                "3. camera\n\t" +
                "4. mobile phone\n\t" +
                "5. jewellery box");
    }

    public static File getFileFromResources(String fileName) {
        ClassLoader classLoader = MenuUtils.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            file = new File(resource.getFile());
        }
        return file;
    }
}
